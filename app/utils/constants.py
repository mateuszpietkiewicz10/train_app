from os import environ

DATABASE_URL = environ.get("DATABASE_URL")
REDIS_URL = environ.get("REDIS_URL")
CONFIG_FILENAME = "config.json"
DEFAULT_NOTIFICATION_HOST = "https://example.com/status-update"
DEFAULT_NOTIFICATION_INTERVAL = 2
LOG_PATH = "train-app/app/logs.log"
