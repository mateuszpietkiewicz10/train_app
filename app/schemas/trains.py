from pydantic import BaseModel
from typing import Optional

from app.utils.enums import TrainType


class TrainModel(BaseModel):
    id: int
    name: str
    type: TrainType
    station_id: Optional[int] = None
