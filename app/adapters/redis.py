from app.utils.constants import REDIS_URL
from app.utils.enums import ApplicationStatus
from redis import Redis


class RedisAdapter:
    def __init__(self, redis_url=REDIS_URL):
        self.__conn: 'Redis[str]' = Redis.from_url(redis_url, decode_responses=True)
        self.set_application_status(ApplicationStatus.STANDBY)

    def set_application_status(self, status: ApplicationStatus):
        return self.__conn.set("ApplicationStatus", status.value)

    def get_application_status(self):
        return self.__conn.get("ApplicationStatus")
