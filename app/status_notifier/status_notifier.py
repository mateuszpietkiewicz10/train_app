import asyncio
import time

import httpx

from app.adapters.redis import RedisAdapter
from app.utils.config import config
from app.utils.logger import logger


class StatusNotifier:
    def __init__(self, loop: asyncio.AbstractEventLoop = None):
        """
        Check and send status of application to the remote host in specified interval
        """
        self._host = config.notification_host
        self._interval = config.notification_interval
        self._stopped = False
        self.loop = loop or asyncio.get_event_loop()
        self.last_notification = None
        self.redis = RedisAdapter()

    def stop(self):
        "This method stops the infinite loop created by run_forever"
        self._stopped = True

    @property
    def stopped(self):
        return self._stopped

    @property
    def interval(self):
        return self._interval

    async def send(self, status: str):
        """
        Send app status to remost host
        """
        try:
            logger.info(f"Sending status '{status}' of application to {self._host}")
            async with httpx.AsyncClient() as client:
                r = await client.post(config.notification_host, json={"status": status})
            logger.info(f"Response status code {r.status_code} and msg {str(r)}")
        except Exception:
            logger.warning(f"Unable to send status")

    async def run_forever(self):
        """
        Infinite loop to send status in interval
        """
        while not self.stopped:
            try:
                if not self.last_notification or time.time() - self.last_notification >= self.interval:
                    await self.send(self.redis.get_application_status())
                    self.last_notification = time.time()
            except Exception as e:
                self.stop()
