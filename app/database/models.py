import uuid

from sqlalchemy.orm import relationship, Mapped, mapped_column, DeclarativeBase
from sqlalchemy import String, ForeignKey, Integer, Float
from typing import List


class Base(DeclarativeBase):
    pass


class TrainStation(Base):
    __tablename__ = "trainstations"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String, nullable=False)
    lat: Mapped[float] = mapped_column(Float, nullable=False)
    lon: Mapped[float] = mapped_column(Float, nullable=False)
    arrival_departure_time: Mapped[int] = mapped_column(Integer, nullable=False)
    trains: Mapped[List['Train']] = relationship(back_populates="station", lazy="selectin")
    # lazy argument to fix greenlet_spawn issue

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "lat": self.lat,
            "lon": self.lon,
            "arrival_departure_time": self.arrival_departure_time,
            "trains": [t.to_dict() for t in self.trains]
        }


class Train(Base):
    __tablename__ = "trains"

    id: Mapped[int] = mapped_column(default=uuid.uuid4, nullable=False, primary_key=True)
    name: Mapped[str] = mapped_column(String, nullable=False)
    type: Mapped[str] = mapped_column(String, nullable=False) # TODO: add enum
    station_id: Mapped[int | None] = mapped_column(ForeignKey("trainstations.id"), nullable=True)
    station: Mapped[TrainStation | None] = relationship(back_populates="trains", lazy="selectin")
    # lazy argument to fix greenlet_spawn issue

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "type": self.type,
        }
