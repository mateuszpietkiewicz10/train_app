from typing import Optional

from pydantic import BaseModel

from app.schemas.trains import TrainModel


class TrainStationModel(BaseModel):
    id: int
    name: str
    lat: float
    lon: float
    arrival_departure_time: int


class ArrivalDepartureModel(BaseModel):
    train_id: int
    station_id: Optional[int] = None
    train: Optional[TrainModel] = None
    train_station: Optional[TrainStationModel] = None
