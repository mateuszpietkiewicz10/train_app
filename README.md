# Train App
Dockerized Async Python web application with data about trains and train stations:
Application have several endpoint:
 - Add station to db
 - Add train to db
 - Train arrival and departure (Specific train deperture/arrival should lasts the same amount of seconds like it has defined in it's record "arrival_departure_time". I used arrival_and_departure decorator for that.)

## Tech stack:
 - Python 3.11
 - PostgreSQL 15.3
 - asyncio
 - asyncpg (Python lib to communicate with db)
 - FastAPI
 - alembic
 - docker
 - docker_compose
 - redis
 - httpx (async requests)
Please read below description with steps how to run application and supported endpoints with example data


## How to run

```
cd existing_repo
docker-compose up -d --build
docker compose exec web alembic upgrade head

To check logs you can use
docker-compose logs status_notifier
```

## Configuration
Application is congirurable from app/config.json file

```
CONFIG
{
  "NOTIFICATION_HOST": "", # External URL to post information about app status. By default "https://example.com/status-update"
  "NOTIFICATION_INTERVAL": "", # Notification interval in seconds. By default 2 seconds
  "DEBUG": true # Logging level
}
```

## Endpoints
## [POST] Add train http://0.0.0.0:8004/api/trains

```
BODY
{   
    "id": 1,
    "name": "train1",
    "type": "electric"
}
```

## [POST] Add train station http://0.0.0.0:8004/api/train_stations

```
BODY
{   
    "id": 1,
    "name": "station1",
    "lat": 54.126737,
    "lon": -32.51353,
    "arrival_departure_time": 10
}
```

## [GET] Get station and its details http://0.0.0.0:8004/api/train_stations/{train_station_id}

```
EXAMPLE RESPONSE
{
    "response": {
        "message": "Train station details",
        "status": "ok",
        "train_station": {
            "id": 1,
            "name": "station1",
            "lat": 54.126737,
            "lon": -32.51353,
            "arrival_departure_time": 10,
            "trains": [
                {
                    "id": 1,
                    "name": "train1",
                    "type": "electric"
                }
            ]
        }
    },
    "status_code": 200
}
```

## [PUT] Train arrival http://0.0.0.0:8004/api/train_stations/train_arrival

```
BODY
{   
    "train_id": 1,
    "station_id": 1
}
```

## [PUT] Train departure http://0.0.0.0:8004/api/train_stations/train_departure

```
BODY
{   
    "train_id": 1
}
```
