from app.database.models import TrainStation
from app.database.utils import DBSessionDep
from app.database.views import get_train_station_db
from fastapi import APIRouter

from app.schemas.train_stations import TrainStationModel, ArrivalDepartureModel
from app.utils.decorators import arrival_and_departure, ok_message, wrap_error
from app.utils.functions import refresh_object_in_db

router = APIRouter(
    prefix="/api/train_stations",
    responses={404: {"description": "Not found"}},
)


@router.post(
    "/",
)
@wrap_error
async def create_train_station(
    train_station: TrainStationModel,
    db_session: DBSessionDep,
):
    """
    Create train station
    """
    train_station = TrainStation(id=train_station.id, name=train_station.name, lat=train_station.lat,
                                 lon=train_station.lon, arrival_departure_time=train_station.arrival_departure_time)
    await refresh_object_in_db(db_session, train_station)
    return ok_message(f"Train station created !", train_station=dict(**train_station.to_dict()))


@router.get(
   "/{train_station_id}",
)
async def get_train_station(
    train_station_id: int,
    db_session: DBSessionDep,
):
    """
    Get train station and based trains
    """
    train_station = await get_train_station_db(db_session, train_station_id)
    return ok_message(f"Train station details", train_station=dict(**train_station.to_dict()))


@router.put(
    "/train_arrival",
)
@arrival_and_departure(is_arrival=True)
async def train_arrival(
    data: ArrivalDepartureModel,
    db_session: DBSessionDep,
):
    """
    Train arrival to provided station (if train is not based in any station)
    """
    train = data.train
    train.station_id = data.train_station.id
    await refresh_object_in_db(db_session, train)
    return ok_message(f"Train {train.name} arrived to station {data.train_station.name}")


@router.put(
    "/train_departure",
)
@arrival_and_departure(is_arrival=False)
async def train_departure(
    data: ArrivalDepartureModel,
    db_session: DBSessionDep,
):
    """
    Train departure from its station (if train is based in any station)
    """
    train = data.train
    train.station_id = None
    await refresh_object_in_db(db_session, train)
    return ok_message(f"Train {train.name} departed from station {data.train_station.name}")
