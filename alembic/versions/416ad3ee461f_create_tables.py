"""create tables

Revision ID: 416ad3ee461f
Revises: 
Create Date: 2024-01-14 10:52:30.891994

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
revision = '416ad3ee461f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'trainstations',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('lat', sa.Float, nullable=False),
        sa.Column('lon', sa.Float, nullable=False),
        sa.Column('arrival_departure_time', sa.Integer),
    )
    op.create_table(
        'trains',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('type', sa.String(50), nullable=False),
    )
    op.add_column('trains', sa.Column('station_id', sa.Integer, nullable=True))
    op.create_foreign_key(None, 'trains', 'trainstations',
                          ['station_id'], ['id'])
    op.create_index(op.f('ix_train_name'), 'trains', ['name'], unique=True)
    op.create_index(op.f('ix_train_station_name'), 'trainstations', ['name'], unique=True)


def downgrade() -> None:
    op.drop_table('trains')
    op.drop_table('trainstations')
