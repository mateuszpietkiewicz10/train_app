import logging
from app.utils.config import config

logging.basicConfig(filename="app/logs.log", encoding="utf-8",
                    level=logging.DEBUG if config.debug else logging.INFO)

logger = logging.getLogger('app')
logger.addHandler(logging.StreamHandler())
