import asyncio
import time
from functools import wraps
from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError

from app.database.views import get_train_db, get_train_station_db
from app.utils.status_manager import StatusManager


def error(message: str, status_code: int, **kwargs):
    return dict(response=dict(message=message, status="error", **kwargs), status_code=status_code)


def ok_message(message: str, **kwargs):
    return dict(response=dict(message=message, status="ok", **kwargs), status_code=int(200))


def wrap_error(func):
    @wraps(func)
    async def decorator(*args, **kwargs):
        with StatusManager():
            try:
                return await func(*args, **kwargs)
            except IntegrityError as e:
                return error(str(e.orig.args[0]), 409)
            except HTTPException as e:
                return error(e.description, e.code)
            except Exception as e:
                return error("Unknown error", 500)

    return decorator


def arrival_and_departure(is_arrival: bool):
    def inner(func):
        @wraps(func)
        async def decorator(*args, **kwargs):
            with StatusManager():
                start = time.time()
                data = kwargs["data"]
                db_session = kwargs["db_session"]
                train_id = data.train_id
                train = await get_train_db(db_session, train_id)
                if not is_arrival and not train.station_id:
                    msg = f"Cannot departure train {train.name}, because it's not based in any station!"
                    return error(msg, 403)
                if is_arrival and train.station_id:
                    msg = f"Train {train.name} is currently on station {train.station.name}!"
                    return error(msg, 403)

                station_id = data.station_id or train.station_id
                train_station = await get_train_station_db(db_session, station_id)
                data.train = train  # Add train to args for future usage to not call db again
                data.train_station = train_station  # Add train station to args for future usage to not call db again
                kwargs["data"] = data
                try:
                    response = await func(*args, **kwargs)
                    stop = time.time()
                    time_left = train_station.arrival_departure_time - (stop - start)
                    if time_left > 0:
                        await asyncio.sleep(time_left)
                except Exception as e:
                    return error(f"Unexpected error occurred {e}", 403)
                return response

        return decorator

    return inner
