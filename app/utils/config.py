import json
from pathlib import Path

from app.utils.constants import CONFIG_FILENAME, DEFAULT_NOTIFICATION_HOST, DEFAULT_NOTIFICATION_INTERVAL

config_path = Path(__file__).parent.parent.joinpath(CONFIG_FILENAME)


class Config:
    def __init__(self, *args, **kwargs):
        notification_interval = kwargs.get("NOTIFICATION_INTERVAL")
        self.notification_host = kwargs.get("NOTIFICATION_HOST") or DEFAULT_NOTIFICATION_HOST
        self.notification_interval = float(notification_interval) if notification_interval else (
            DEFAULT_NOTIFICATION_INTERVAL)
        self.debug = kwargs.get("DEBUG", False)


with open(config_path) as config_json:
    config_dict = json.load(config_json)
    config = Config(**config_dict)
