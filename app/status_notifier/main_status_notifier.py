from app.status_notifier.status_notifier import StatusNotifier

if __name__ == "__main__":
    sf = StatusNotifier()
    sf.loop.run_until_complete(sf.run_forever())
    sf.loop.close()
