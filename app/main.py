
from contextlib import asynccontextmanager

import uvicorn
from app.database.utils import sessionmanager
from app.routers.trains import router as trains_router
from app.routers.train_stations import router as trains_stations_router
from fastapi import FastAPI


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Function that handles startup and shutdown events.
    """
    yield
    if sessionmanager._engine is not None:
        # Close the DB connection
        await sessionmanager.close()


app = FastAPI(lifespan=lifespan, title="title", docs_url="/api/docs")

# Routers
app.include_router(trains_router)
app.include_router(trains_stations_router)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", reload=True, port=8000)