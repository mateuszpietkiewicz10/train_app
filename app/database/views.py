from app.database.models import TrainStation, Train
from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession


async def get_train_station_db(db_session: AsyncSession, train_station_id: int):
    trains_station = (await db_session.scalars(select(TrainStation).where(TrainStation.id == train_station_id))).first()
    if not trains_station:
        raise HTTPException(status_code=404, detail="Train station not found")
    return trains_station


async def get_train_db(db_session: AsyncSession, train_id: int):
    train = (await db_session.scalars(select(Train).where(Train.id == train_id))).first()
    if not train:
        raise HTTPException(status_code=404, detail="Train not found")
    return train
