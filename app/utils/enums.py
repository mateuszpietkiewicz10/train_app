from enum import Enum


class TrainType(Enum):
    STEAM = "steam"
    ELECTRIC = "electric"
    DIESEL = "diesel"


class ApplicationStatus(Enum):
    STANDBY = "standby"
    BUSY = "busy"
