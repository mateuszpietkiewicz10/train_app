from app.database.models import Train
from app.database.utils import DBSessionDep
from app.schemas.trains import TrainModel
from fastapi import APIRouter

from app.utils.decorators import ok_message, wrap_error
from app.utils.functions import refresh_object_in_db

router = APIRouter(
    prefix="/api/trains",
    responses={404: {"description": "Not found"}},
)


@router.post(
    "/",
)
@wrap_error
async def create_train(
    train: TrainModel,
    db_session: DBSessionDep,
):
    """
    Create train
    """
    train = Train(id=train.id, name=train.name, type=train.type.value, station_id=train.station_id)
    await refresh_object_in_db(db_session, train)
    return ok_message(f"Train created !", train=dict(**train.to_dict()))
