from app.adapters.redis import RedisAdapter
from app.utils.enums import ApplicationStatus


class StatusManager:
    """
    Context manager to update status of application
    """
    def __init__(self):
        self.redis = RedisAdapter()

    def __enter__(self):
        self.redis.set_application_status(ApplicationStatus.BUSY)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.redis.set_application_status(ApplicationStatus.STANDBY)
