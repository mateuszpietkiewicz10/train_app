async def refresh_object_in_db(db_session, train):
    db_session.add(train)
    await db_session.commit()
    await db_session.refresh(train)
